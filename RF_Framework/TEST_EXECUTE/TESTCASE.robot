*** Settings ***
Suite Setup       REPORT SET
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ========================================================================================================================================
Resource          TEST_KEYWORD.txt

*** Test Cases ***
TC001_AdvantageDEMO
    RUN_EXECUTE    ${TEST_NAME}

TC09_TEST
    [Setup]
    Log To Console    <<<< START >>>>
    [W] Open Browser    https://demostore.x-cart.com/?fbclid=IwAR3Risac4MFaThOrnDBwwabgLgpC-U1RHW1AOSdg_TS278XVNUWtIr3rr94
    [W] Click Element    //div[contains(@class,'header_bar-sign_in')]//span[contains(text(),'Sign in')]
    Sleep    2
    [W] Input Text    //input[@id='login-email']    patipan.t@extosoft.com
    [W] Input Text    //input[@id='login-password']    patipan@1234
    [W] Click Element    //button[contains(@class,'submit')]//span[contains(text(),'Sign in')]
    Sleep    5
    [W] Click Element    //ul[@class='nav navbar-nav top-main-menu']//span[contains(text(),'New!')]
    [W] Click Element    //span[contains(text(),'Electronics')]
    [W] Click Element    //span[@class='subcategory-name'][contains(text(),'Mobile phones')]
    Sleep    5
    [W] Click Element    //a[@class='fn url'][contains(text(),'Apple iPhone 8 Plus, 64GB')]
    [W] Select From List By Value    //*[@name="attribute_values[73]"]    Gold
    [W] Select From List By Value    //*[@name="attribute_values[71]"]    256
    [W] Click Element    //button[contains(@class,'regular-button regular-main-button add2cart submit')]//span[contains(text(),'Add to cart')]
    [W] Click Element    //div[contains(@class,'item-buttons')]//span[contains(text(),'View cart')]
    [W] Click Element    //span[contains(text(),'Go to checkout')]
    comment    [W] Input Text    //order-notes[@class='floating-label secondary focused']//textarea[@id='order_note']    TESTTEST
    [W] Click Element    //button[@class='btn regular-button regular-main-button checkout_fastlane_section-next']
    comment    [W] Select Radio Button    //input[@id='pmethod6']
    [W] Click Element    //button[@class="btn regular-button regular-main-button checkout_fastlane_section-place_order place-order"]
    comment    [W] Wait Until Page Contains    //*[@id="page-title"]
    [W] Click Element    //a[contains(text(),'My account')]
    [W] Click Element    //ul[@class='account-links dropdown-menu']//span[contains(text(),'Log out')]
    Log To Console    <<<< END >>>>